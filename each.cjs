function each(arr,cb){
    if(Array.isArray(arr))
    {
      for(let indx = 0;indx < arr.length;indx++)
      {
         let value = cb(arr[indx],indx,arr);
         arr[indx] = value;
       }
       return arr;
    }else{
        return []
    }
}
module.exports = each;