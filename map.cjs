function map(arr,cb){
  let list = [];
    if(Array.isArray(arr)){
      for(let indx = 0;indx < arr.length;indx++){
         list.push(cb(arr[indx],indx,arr));
       }
       return list;
    }else{
        return []
    }
}
module.exports = map;