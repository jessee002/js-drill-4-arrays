function filter(arr,cb, ele)
{
    if(typeof(cb) == 'function' && arr.length != 0)
    {
      const num = [];
        for(let idx = 0;idx < arr.length;idx++)
        {
          if(cb(arr[idx],idx,arr) === true){
             num.push(arr[idx]);
        }
      }
      return num;
    }
    else{
        return [];   
    }
}   
module.exports=filter;