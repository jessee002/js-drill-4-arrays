function flatten(arr, depth) {
  if (!Array.isArray(arr)) {
      return []
  }
  if (depth === undefined) {
       depth = 1
  }

  let flatArr = []
  for (let index = 0; index < arr.length; index++)
  {
      if (Array.isArray(arr[index]) && depth > 0)
      {
          flatArr = flatArr.concat(flatten(arr[index], depth - 1))
      } else if (arr[index] === undefined || arr[index] === null)
      {
          continue
      }
      else {
          flatArr.push(arr[index])
      }
  }
  return flatArr
}
module.exports = flatten
