function reduce(arr, cb, startingValue)
{
    let startIdx = 0
    if(startingValue == undefined)
    {
        startingValue = arr[0];
        startIdx = 1;
    }
    if(typeof cb != 'function')
    {
        return [];
    }
    else
    {
      for(let idx = startIdx; idx < arr.length; idx++){
        startingValue = cb(startingValue,arr[idx],idx,arr);
        }
    return startingValue;
    }
}
module.exports=reduce;