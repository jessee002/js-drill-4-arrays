function find(arr,cb)
{
    if(typeof cb == 'function')
    {
      let num = [];
        for(let indx = 0;indx < arr.length;indx++)
        {
          const val = cb(arr[indx],indx,arr);
          //console.log(val);
          if(val) return (arr[indx]);
        }
    }else{
        return undefined;   
    }
}  
module.exports=find;